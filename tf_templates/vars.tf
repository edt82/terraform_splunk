/*
variable "GOOGLE_PROJECT_ID" {}
variable "CUSTOMER_DEPLOYMENT_PLATFORM" {}
variable "GOOGLE_APPLICATION_CREDENTIALS" {}
*/

variable "CUSTOMER" {
    default = "lpt82"
}

variable "CUSTOMER_ENVIRONMENT" {
    default = "blue"
}


# Prefix used for naming machines
variable "prefix" {
  default = "splunk-terraform"
}

variable "CUSTOMER_DEPLOYMENT_REGION" {
}

variable "CUSTOMER_DEPLOYMENT_REF" {
    default="helloworld"
}

variable "prod_machine_type" {
  description = "Which instance type should be used"
  default     = "n1-standard-4"
}

variable "demo_machine_type" {
  description = "Which instance type should be used"
  default     = "n1-standard-1"
}

variable "gce_image" {
  description = "This is the AMI image that will be used for the builds."
  default     = "centos-7-v20190619"
}

variable "idx_prod_disk_size" {
  description = "Hot and Cold volume size in GB for prod"

  default = {
    "hot"  = 50
    "cold" = 100
  }
}

variable "idx_demo_disk_size" {
  description = "Hot and Cold volume size in GB for demo"

  default = {
    "hot"  = 10
    "cold" = 10
  }
}

variable "deployment_purpose" {
  description = "Purpose of this deployments ('prod' or 'demo')"
  default     = "demo"
}

variable "indexing_cluster_node_count" {
  description = "Count of Splunk Indexing Cluster nodes"
  default     = 2
}

variable "gcp_ssh_user" {}

# Path to your public SSH key
variable "ssh_key_path" {
  default = "~/.ssh/id_rsa.pub"
}

# URL for required Splunk tar
variable "splunk_download" {
  default = "https://www.splunk.com/bin/splunk/DownloadActivityServlet?architecture=x86_64&platform=linux&version=7.2.6&product=splunk&filename=splunk-7.2.6-c0bf0f679ce9-Linux-x86_64.tgz&wget=true"
}

# Splunk password and pass4SymmKey
variable "splunk_password" {}

# Number of indexers to be spawned
variable "num_indexers" {
  default = 2
}

# Number of Splunk sites to be configured
variable "num_sites" {
  default = 1
}

variable "gcp_project" {}
variable "gcp_region" {
  default = "australia-southeast1"
}


variable "splunk_volumes" {
    type = "map"
    default = {
        "hot" = {
            "size" = -1
            "path" = ""
        }
        "cold" = {
            "size" = -2
            "path" = ""
        }
    }
}
variable "splunk_ports" {
    type = "map"
    default = {
        splunkd = 8089
        hec = 8088
        forwarder = 9997
        webconsole = 8000
        index_replication = 8080
        kv_store = 8191
    }

}