REPO_ROOT=/tmp/terraform_splunk
yum install git ansible -y
git clone --depth 1 https://bitbucket.org/edt82/terraform_splunk.git $REPO_ROOT
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook \
-e "prefix=$PREFIX" \
-e "splunk_password=\"$SPLUNK_PASSWORD\"" \
-e "splunk_download=\"$SPLUNK_DOWNLOAD\"" \
-e "icm_hostname=$ICM_HOSTNAME" \
$REPO_ROOT/ansible/master.yml -t $SPLUNK_ROLE -e "@$REPO_ROOT/vars/overrides.json"
