provider "google" {
  project     = "${var.project}"
  region      = "australia-southeast1"
}

provider "google-beta" {
  project     = "${var.project}"
  region      = "australia-southeast1"
}

data "google_compute_network" "default-network" {
  name = "default"
}

data "google_compute_zones" "available" {
  status = "UP"
  project     = "${var.project}"
}

data "http" "client_ip" {
  url = "https://ifconfig.co"
}

#=============
/*
resource "google_compute_instance" "icm" {
  name = "${var.prefix}-icm"
  tags = ["splunkd","splunk", "splunkweb"]
  machine_type = "${var.deployment_purpose == "prod" ? var.prod_machine_type : var.demo_machine_type }"
  zone         = "${data.google_compute_zones.available.names[0]}"
  allow_stopping_for_update = true

  service_account {
      scopes = ["storage-full"]
      email = "icmservice@terraform01-255007.iam.gserviceaccount.com"
  }

  boot_disk {
    initialize_params {
      image = "${var.gce_image}"
    }
  }

metadata_startup_script = <<EOF
#! /bin/bash
SPLUNK_ROLE=icm
SPLUNK_PASSWORD=${var.splunk_password}
SPLUNK_DOWNLOAD="${var.splunk_download}"
PREFIX=${var.prefix}
#ICM_IP="$(curl -H 'Metadata-Flavor: Google' http://metadata.google.internal/computeMetadata/v1/instance/network-interfaces/0/ip)"
ICM_HOSTNAME=icm.${google_dns_managed_zone.dnszone.dns_name}
${file("../scripts/startup.sh")}
EOF

 network_interface {
    # A default network is created for all GCP projects
    network       = "default"
    access_config {}
  }

  metadata = {
    ssh-keys = "${var.gcp_ssh_user}:${file(pathexpand(var.ssh_key_path))}"
  }

  lifecycle {
    create_before_destroy = true
  }

}
*/

########################
resource "google_compute_instance_template" "indexer" {
  name_prefix        = "indexer-template"
  tags = ["splunkd", "splunk", "splunkweb", "splunkhec"]
  machine_type = "${var.deployment_purpose == "prod" ? var.prod_machine_type : var.demo_machine_type }"
  
  lifecycle {
    create_before_destroy = true
  }

  labels = {
    deployment_ref   = "${var.CUSTOMER_DEPLOYMENT_REF}"
    roletags         = "splunk_idx"
    #multi_site_label = "${data.google_compute_zones.available.names[count.index]}"
    cluster_label    = "primidx"
  }
 
   metadata = {
    ssh-keys = "${var.gcp_ssh_user}:${file(pathexpand(var.ssh_key_path))}"
  }

  metadata_startup_script = <<EOF
#! /bin/bash
yum install certbot -y
SPLUNK_ROLE=idx
SPLUNK_PASSWORD=${var.splunk_password}
SPLUNK_DOWNLOAD="${var.splunk_download}"
PREFIX=${var.prefix}
ICM_HOSTNAME=icm-ilb.group2-ilb.il4.australia-southeast1.lb.terraform01-255007.internal
${file("../scripts/startup.sh")}
EOF

  disk {
    boot = true
    auto_delete = true
    source_image = "${var.gce_image}" 
  }

  disk {
    boot = false
    auto_delete = true
    source_image = "lpt82-blank-1"
#    disk_size_gb = "${var.splunk_volumes["hot"]["size"]}"
    device_name = "splunk-hot"
    mode        = "READ_WRITE"
  }

  disk {
    boot = false
    auto_delete = true
    source_image = "lpt82-blank-1"
#    disk_size_gb = "${var.splunk_volumes["cold"]["size"]}"
    device_name = "splunk-cold"
    mode        = "READ_WRITE"
  }

  network_interface {
    # A default network is created for all GCP projects
    network       = "default"
    access_config {}
  }

}

/*
resource "google_compute_disk" "splunk-idx-hot" {
  count = "${var.indexing_cluster_node_count}"
  name  = "splunk-idx-hot-${count.index}"
  type  = "pd-ssd"
  #zone  = "${count.index < floor(var.indexing_cluster_node_count * 0.5) ? var.az1 : var.az2 }"
  size  = "${var.deployment_purpose == "prod" ? var.idx_prod_disk_size["hot"]: var.idx_demo_disk_size["hot"] }"

  labels = {
    deployment_ref = "${var.CUSTOMER_DEPLOYMENT_REF}"
  }

  physical_block_size_bytes = 4096
}

resource "google_compute_disk" "splunk-idx-cold" {
  count = "${var.indexing_cluster_node_count}"
  name  = "splunk-idx-cold${count.index}"
  type  = "pd-standard"
  #zone  = "${count.index < floor(var.indexing_cluster_node_count * 0.5) ? var.az1 : var.az2 }"
  size  = "${var.deployment_purpose == "prod" ? var.idx_prod_disk_size["cold"] : var.idx_demo_disk_size["cold"] }"

  labels = {
    deployment_ref = "${var.CUSTOMER_DEPLOYMENT_REF}"
  }

  physical_block_size_bytes = 4096
}
*/

resource "google_compute_health_check" "autohealing" {
  name                = "autohealing-health-check"
  check_interval_sec  = 5
  timeout_sec         = 5
  healthy_threshold   = 2
  unhealthy_threshold = 5
  
 tcp_health_check {
   port = "8089"
 }
}

resource "google_compute_region_instance_group_manager" "idxcluster" {
  provider = "google-beta"
  name = "idxcluster-igm"

  base_instance_name         = "idx"
  region                     = "${var.CUSTOMER_DEPLOYMENT_REGION}"
  #distribution_policy_zones  = ["us-central1-a", "us-central1-f"]

  version {
    name = "test1"
    instance_template = "${google_compute_instance_template.indexer.self_link}"
  }
  #target_pools = ["${google_compute_target_pool.appserver.self_link}"]
  target_size  = "${var.num_indexers}"

  named_port {
    name = "forwarding"
    port = 9997
  }

  named_port {
    name = "splunkd"
    port = 8089
  }

  named_port {
    name = "splunkhec"
    port = "${var.splunk_ports["hec"]}"
  }

  # requires google beta provider
  auto_healing_policies {
    health_check      = "${google_compute_health_check.autohealing.self_link}"
    initial_delay_sec = 180
  }

}

// add autoscale group for HA of instances and allow updates to template definition. instance replacement is not supported 
// by terraform which is a basic requirmement is really disappointing


# ---------------------------------------------------------------------------------------------------------------------
# external SSL load balancer for HEC on IDX cluster
# ---------------------------------------------------------------------------------------------------------------------

module "hec-lb" {
  source         = "./terraform-google-lb"
  region         = "australia-southeast1"
  name           = "hec-lb1"
  service_port   = "${var.splunk_ports["hec"]}"
  target_tags    = ["splunkhec"]
  backend_service = "${google_compute_region_instance_group_manager.idxcluster.self_link}"
}

################# firewall rules ###################
resource "google_compute_firewall" "splunkweb" {
  name    = "allow-splunkweb"
  network = "default"


  allow {
    protocol = "tcp"
    ports    = ["${var.splunk_ports["webconsole"]}"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags = ["splunkweb"]
}

resource "google_compute_firewall" "splunkd" {
  name    = "allow-splunkd"
  network = "default"


  allow {
    protocol = "tcp"
    ports    = ["${var.splunk_ports["splunkd"]}"]
  }

  source_ranges = ["130.211.0.0/22","35.191.0.0/16"]
  source_tags = ["splunkd"]
  target_tags = ["splunkd"]
}

resource "google_compute_firewall" "splunkhec" {
  name    = "allow-splunkhec"
  network = "default"


  allow {
    protocol = "tcp"
    ports    = ["${var.splunk_ports["hec"]}"]
  }

  source_ranges = ["0.0.0.0/0"]
  source_tags = ["splunk"]
  target_tags = ["splunkhec"]
}

#======== DNS ==================

data "google_dns_managed_zone" "gcp-techphlex-com" {
  name        = "gcp-techphlex-com"
}

/*
resource "google_dns_managed_zone" "dnszone" {
  name     = "${var.CUSTOMER_ENVIRONMENT}-${var.CUSTOMER}"
  dns_name = "${var.CUSTOMER_ENVIRONMENT}.${var.CUSTOMER}.datapaas."

  visibility = "private"

  private_visibility_config {
    networks {
      network_url = "${data.google_compute_network.default-network.self_link}"
    }
  }
}
*/

resource "google_dns_record_set" "hec_lb_dns" {
  name = "splunkhec.${data.google_dns_managed_zone.gcp-techphlex-com.dns_name}"
  type = "A"
  ttl  = 300

  managed_zone = "${data.google_dns_managed_zone.gcp-techphlex-com.name}"

  rrdatas = ["${module.hec-lb.external_ip}"]
}
