#
# TLS Certificate the ICM external and internal http load balancer
# DNS for the load balancer
# startup script to kick off ansible playbook for master configuration and restore of snapshpts or cloud storage
#

# ---------------------------------------------------------------------------------------------------------------------
# cluster master internal load balancer for splunkd management traffic
# ---------------------------------------------------------------------------------------------------------------------
module "gce-ilb" {
  source         = "./terraform-google-lb-internal"
  region         = "australia-southeast1"
  name           = "group2-ilb"
  service_label  = "icm-ilb"
  ports          = ["${var.splunk_ports["splunkd"]}"]
  health_port    = "${var.splunk_ports["splunkd"]}"
  source_tags    = ["splunk"]
  target_tags    = ["splunkclustermaster"]
  backends       = [
      # https://github.com/hashicorp/terraform/issues/4336#issuecomment-255327006 - GCP API returns incorrect IGM url - this is monkey patch
    { group = "${replace(google_compute_region_instance_group_manager.clustermaster.self_link, "Manager", "")}" },
  ]
}

# ---------------------------------------------------------------------------------------------------------------------
# Index Cluster Master Definition
# ---------------------------------------------------------------------------------------------------------------------

resource "google_compute_instance_template" "clustermaster" {
  name_prefix         = "clustermaster-template"
  machine_type = "${var.deployment_purpose == "prod" ? var.prod_machine_type : var.demo_machine_type }"

  lifecycle {
    create_before_destroy = true
  }

  service_account {
      scopes = ["storage-full"]
      email = "icmservice@terraform01-255007.iam.gserviceaccount.com"
  }

  tags = ["splunkd","splunk", "splunkweb", "splunkclustermaster"]

  labels = {
    name             = "clustermaster"
    deployment_ref   = "${var.CUSTOMER_DEPLOYMENT_REF}"
    roletags         = "splunk_cm"
    multi_site_label = "site1"
    cluster_label    = "primidx"
    deployer_label   = "main"
  }

  # OS disk
  disk {
    boot = true
    auto_delete = true
    source_image = "${var.gce_image}" 
  }

  # Splunk install disk
  disk {
    boot = false
    auto_delete = true
    source_image = "splunk"
    device_name = "splunk-home"
    mode        = "READ_WRITE"
  }

  network_interface {
    # A default network is created for all GCP projects
    network       = "default"
    access_config {}
  }


  metadata_startup_script = <<EOF
#! /bin/bash

# schedule backup for splunk cluster master configs


SPLUNK_ROLE=icm
SPLUNK_PASSWORD=${var.splunk_password}
SPLUNK_DOWNLOAD="${var.splunk_download}"
PREFIX=${var.prefix}
#ICM_IP="$(curl -H 'Metadata-Flavor: Google' http://metadata.google.internal/computeMetadata/v1/instance/network-interfaces/0/ip)"
ICM_HOSTNAME=127.0.0.1
${file("../scripts/startup.sh")}
EOF

}

# ---------------------------------------------------------------------------------------------------------------------
# IGM index cluster master instance group manager
# ---------------------------------------------------------------------------------------------------------------------

resource "google_compute_region_instance_group_manager" "clustermaster" {
  provider = "google-beta"
  name = "clustermaster-igm"

  base_instance_name         = "clustermaster"
  region                     = "${var.CUSTOMER_DEPLOYMENT_REGION}"
  #distribution_policy_zones  = ["us-central1-a", "us-central1-f"]

  version {
    name = "test1"
    instance_template = "${google_compute_instance_template.clustermaster.self_link}"
  }

  #target_pools = ["${google_compute_target_pool.appserver.self_link}"]
  target_size  = 1

  named_port {
    name = "splunkweb"
    port = "${var.splunk_ports["webconsole"]}"
  }

  named_port {
    name = "splunkd"
    port = "${var.splunk_ports["splunkd"]}"
  }


  # requires google beta provider
  auto_healing_policies {
    health_check      = "${google_compute_health_check.autohealing.self_link}"
    initial_delay_sec = 120
  }
}


#==================================================================================
# HTTP load balancer
# update the backend resource so that it can use the same instance group manager
#==================================================================================

module "gce-lb-http" {
  source            = "./terraform-google-lb-http"
  name              = "icm-lb"
  project           = "${var.project}"
  target_tags       = ["splunkclustermaster"]
  firewall_networks = ["${data.google_compute_network.default-network.name}"]
  ssl               = true
  certificate       = "${file("../../vars/master.cert.pem")}"
  private_key       = "${file("../../vars/master.key.pem")}"
  backend_protocol  = "HTTPS"
  http_forward      = false

  backends = {
    "0" = [
      {
        group = "${replace(google_compute_region_instance_group_manager.clustermaster.self_link, "Manager", "")}"
        balancing_mode = "RATE"
        max_rate_per_instance = 100
      },
    ]
  }

  backend_params = [
    // health check path, port name, port number, timeout seconds.
    "/en-GB/account/login,splunkweb,8000,10",
  ]
}


#==================================================================================
# DNS
#==================================================================================

resource "google_dns_record_set" "clustermaster_dns" {
  name = "splunkmaster.${data.google_dns_managed_zone.gcp-techphlex-com.dns_name}"
  type = "A"
  ttl  = 300

  managed_zone = "${data.google_dns_managed_zone.gcp-techphlex-com.name}"

  rrdatas = ["${module.gce-lb-http.external_ip}"]
}


# create backend service
# creat ssl certificate
# create https proxy
# TODO update dns with load balancer external ip

# TODO: 
# - investigate internal http load balancer
# - include indexer instances after instances are recreated by the autohealing policies
# 
